#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
#
# This finds and runs the upstream testsuite for rocRAND
#

# include beakerlib
. /usr/share/beakerlib/beakerlib.sh

PACKAGE='rocrand'
UPSTREAM='rocRAND'

rlJournalStart
    rlPhaseStart FAIL rocminfo
        rlRun -s "rocminfo" 0 "gathering rocminfo"
    rlPhaseEnd

    rlPhaseStartSetup
        # only run dnf install if user is root
        if [ "$EUID" -ne 0 ]
        then
            echo "Not running test as root, skipping package install and assuming that required packages are already installed"
        else
            rlRun "dnf -y builddep $package"
            rlRun "dnf -y install $package-devel"
        fi

        # make sure that the devel pacakge is installed so we can build tests against it
        rlAssertRpm "$PACKAGE-devel"

        # find version of installed rpm so we clone the right branch
        # this won't work with all packages but it does for the packages we care about
        # the rocm upstream branch naming drops the patch version if it's 0.
        # make the assumption that the tests aren't going to change if it's a patch > 0 and drop it
        rlRun "PACKAGE_VERSION=`rpm -q $PACKAGE |  grep -Eo '[0-9]+\.[0-9]+'`"
        rlLog "Building tests against version (ignoring patch) $ROCFFT_VERSION"

        # clone rocFFT code
        if [ -d $UPSTREAM ]
        then
            rlLog "Git checkout dir $UPSTREAM already exists"
        else
            rlRun "git clone https://github.com/ROCmSoftwarePlatform/$UPSTREAM.git"
        fi

        rlAssertExists $UPSTREAM

        # move into the git checkout dir
        pushd $UPSTREAM

        # checkout correct branch for the package installed
        rlRun "git checkout release/rocm-rel-$PACKAGE_VERSION"

    rlPhaseEnd

    rlPhaseStart FAIL buildTests
        # run cmake
        rlRun "cmake . -DBUILD_TEST=ON -DCMAKE_CXX_COMPILER=hipcc -DROCM_SYMLINK_LIBS=OFF -DBUILD_FILE_REORG_BACKWARD_COMPATABILITY=OFF -DBUILD_HIPRAND=OFF"

        # build the tests
        pushd test
        make -j
        rlAssertExists test_rocrand_basic
    rlPhaseEnd

    rlPhaseStart FAIL runTests

        rlRun "ctest --output-junit rocrand-ctest.junit"
        rlAssertExists "Testing/Temporary/LastTest.log"
        rlFileSubmit "Testing/Temporary/LastTest.log"
        rlAssertExists "rocrand-ctest.junit"
        rlFileSubmit "rocrand-ctest.junit"

    rlPhaseEnd

rlJournalPrintText
rlJournalEnd
