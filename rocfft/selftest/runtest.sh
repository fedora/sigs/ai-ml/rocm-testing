#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
#
# Run the upstream testsuite for rocfft
#

# include beakerlib
. /usr/share/beakerlib/beakerlib.sh

PACKAGE='rocfft-test'

rlJournalStart
    rlPhaseStart FAIL rocminfo
        rlRun -s "rocminfo" 0 "gathering rocminfo"
    rlPhaseEnd

    rlPhaseStartSetup
        # only run dnf install if user is root
        if [ "$EUID" -ne 0 ]
        then
            echo "Not running test as root, skipping package install and assuming that required packages are already installed"
        else
            rlRun "dnf -y copr enable tflink/rocm-test-next"
            rlRun "dnf -y install $PACKAGE"
        fi

        # make sure that the package is available
        rlAssertRpm "$PACKAGE"

    rlPhaseEnd

    rlPhaseStart FAIL runTests

        rlRun "rocfft-test --gtest_brief=1 --gtest_output=xml:./rocfft-test-output.xml"
        rlAssertExists "rocfft-test-output.xml"

    rlPhaseEnd

rlJournalPrintText
rlJournalEnd
