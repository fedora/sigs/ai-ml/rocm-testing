# ROCm Testing

This repository houses the tests meant for ROCm in Fedora


# Requirements

You will need `tmt` and `beakerlib` in addition to the dependencies of the individual tests.

# Running tests

Remember that these tests **Require** ROCm capable hardware to pass. Running the tests in a VM is not going to pass.

```
$ tmt --verbose run --all provision --how local
```

The output will show up in `/var/tmp/tmt`
